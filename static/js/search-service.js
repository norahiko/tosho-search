"use strict";

var searchService = {};

(function() {
    var targets = searchService.targets = [];

    searchService.search = function(id, keyword, callback) {
        targets.forEach(function(lib) {
            if(lib.id === id) {
                lib.search(keyword, callback);
            }
        });
    };

    function Target(id, name, url, disabled) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.disabled = !!disabled;
        this.checked = false;
    }

    Target.prototype.search = function(keyword, callback) {
        var key = this.id + "/" + keyword;
        var cache = util.getCache(key, true);
        if(cache) return callback(cache);


        var xhr = new XMLHttpRequest();
        var path = "/search";
        path += "?id=" + this.id;
        path += "&keyword=" + encodeURIComponent(keyword);
        xhr.open("GET", path, true);

        xhr.onload = xhr.onerror = function() {
            var result = JSON.parse(xhr.responseText);
            if(result.ok) {
                util.setCache(key, result, true);
            }
            callback(result);
        };
        xhr.send();
    };

    targets.push(new Target("adachi", "足立区", "https://www.lib.adachi.tokyo.jp/"));
    targets.push(new Target("arakawa", "荒川区", "http://www.library.city.arakawa.tokyo.jp/"));
    targets.push(new Target("itabashi", "板橋区", "https://www.lib.city.itabashi.tokyo.jp/"));
    targets.push(new Target("edogawa", "江戸川区", "https://www.library.city.edogawa.tokyo.jp/", true));
    targets.push(new Target("oota", "大田区", "http://www.lib.city.ota.tokyo.jp/index.html"));
    targets.push(new Target("katsushika", "葛飾区", "http://www.lib.city.katsushika.lg.jp/"));
    targets.push(new Target("kita", "北区", "http://www.library.city.kita.tokyo.jp/"));
    targets.push(new Target("koto", "江東区", "http://www.koto-lib.tokyo.jp/"));
    targets.push(new Target("shinagawa", "品川区", "https://lib.city.shinagawa.tokyo.jp/", true));
    targets.push(new Target("shibuya", "渋谷区", "http://www.lib.city.shibuya.tokyo.jp/hp/", true));
    targets.push(new Target("shinjuku", "新宿区", "http://www.city.shinjuku.lg.jp/library/"));
    targets.push(new Target("suginami", "杉並区", "https://www.library.city.suginami.tokyo.jp/"));
    targets.push(new Target("sumida", "墨田区", "http://www.library.sumida.tokyo.jp/"));
    targets.push(new Target("setagaya", "世田谷区", "http://libweb.city.setagaya.tokyo.jp/"));
    targets.push(new Target("chuou", "中央区", "http://www.library.city.chuo.tokyo.jp/"));
    targets.push(new Target("chiyoda", "千代田区", "http://www.library.chiyoda.tokyo.jp/", true));
    targets.push(new Target("toshima", "豊島区", "http://www.library.toshima.tokyo.jp/"));
    targets.push(new Target("nakano", "中野区", "http://www3.city.tokyo-nakano.lg.jp/"));
    targets.push(new Target("nerima", "練馬区", "https://www.lib.nerima.tokyo.jp"));
    targets.push(new Target("bunkyo", "文京区", "http://www.lib.city.bunkyo.tokyo.jp/"));
    targets.push(new Target("minato", "港区", "https://www.lib.city.minato.tokyo.jp/", true));
    targets.push(new Target("meguro", "目黒区", "http://www.meguro-library.jp/"));
    targets.push(new Target("musashino", "武蔵野市", "http://www.library.musashino.tokyo.jp/"));
    targets.push(new Target("mitaka", "三鷹市", "http://www.library.mitaka.tokyo.jp/"));
    targets.push(new Target("tama", "多摩市", "http://www.library.tama.tokyo.jp/"));
    targets.push(new Target("chofu", "調布市", "https://www.lib.city.chofu.tokyo.jp/"));
    targets.push(new Target("nishitokyo", "西東京市", "http://www.library.city.nishitokyo.lg.jp/index"));
    targets.push(new Target("hachioji", "八王子市", "http://www.library.city.hachioji.tokyo.jp/"));
    targets.push(new Target("higashikurume", "東久留米市", "https://www.lib.city.higashikurume.lg.jp/"));
    targets.push(new Target("fuchu", "府中市", "http://library.city.fuchu.tokyo.jp/"));
})();
