var tosho = {};
"use strict";
(function() {
tosho.targets = null;

tosho.currentRequestId = 0;

tosho.searching = false;

tosho.main = function() {
    window.addEventListener("hashchange", tosho.refleshSearch);

    tosho.models = {
        keyword: "",
        targets: searchService.targets,
        onSubmit: tosho.onSubmit,
        results: [],
        showTargets: true,
        random: function() { return Math.random().toString() },
    };
    tosho.initTargets();
    tosho.view = rivets.bind(document.body, tosho.models);
    tosho.refleshSearch();
    document.getElementById("search-input").focus();
};

tosho.initTargets = function() {
    var checkedTargets = util.getCache("checkedTargets") || {};
    tosho.models.targets.forEach(function(target) {
        if(target.name in checkedTargets) {
            target.checked = true;
            tosho.models.showTargets = false;
        }
    });
};

tosho.refleshSearch = function() {
    var hash = location.hash.slice(1);
    if(hash === tosho.models.keyword) return;
    tosho.models.keyword = hash;
    tosho.startSearch(hash);
};

tosho.onSubmit = function(event) {
    if("preventDefault" in event) {
        event.preventDefault();
    } else {
        event.returnValue = false;
    }
    var keyword = tosho.models.keyword;
    if(keyword === "") return;


    location.hash = keyword;
    var len = tosho.startSearch(keyword);
    if(len) {
        tosho.models.showTargets = false;
    }

    tosho.saveCheckedTargets();
};

tosho.saveCheckedTargets = function() {
    var cache = {};
    tosho.models.targets.forEach(function(target) {
        if(target.checked) {
            cache[target.name] = true;
        }
    });
    util.setCache("checkedTargets", cache);
};

tosho.startSearch = function(keyword) {
    if(keyword === "") return;
    tosho.currentRequestId += 1;
    var id = tosho.currentRequestId;
    var targets = tosho.models.targets.filter(function(target) {
        return target.checked;
    });

    tosho.models.results = [];
    targets.forEach(function(target, i) {
        tosho.models.results.push({
            name: target.name,
            books: [],
            loading: true,
            error: null,
            empty: false,
        });

        searchService.search(target.id, keyword, function(result) {
            if(id !== tosho.currentRequestId) return;

            var model = tosho.models.results[i];
            tosho.renderResults(id, keyword, model, result);
        });
    });
    return targets.length;
};

tosho.renderResults = function(id, keyword, model, result) {
    model.loading = false;

    if(result.searchURL) {
        model.searchURL = result.searchURL;
    }

    if(result.error) {
        model.error = result.error;
        return;
    }

    if(result.books.length === 0) {
        model.empty = true;
        return;
    }

    model.books = result.books;
};


window.addEventListener("load", tosho.main);
})();
