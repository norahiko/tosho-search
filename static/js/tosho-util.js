"use strict";

var util = {};

util.setCache = function(key, value, session) {
    var storage = session ? window.sessionStorage : window.localStorage;
    try {
        storage.removeItem(key);
        storage.setItem(key, JSON.stringify(value));
    } catch(e) {
        console.error(e);
    }
};

util.getCache = function(key, session) {
    var storage = session ? window.sessionStorage : window.localStorage;
    try {
        return JSON.parse(storage.getItem(key));
    } catch(e) {
        console.error(e);
    }
};

util.getScrollTop = function() {
    return document.body.scrollTop || document.documentElement.scrollTop || 0;
};

rivets.formatters.not = function(value) {
    return !value;
};

rivets.formatters.defined = function(value) {
    return value !== null && value !== undefined;
};

rivets.formatters.mediaIcon = function(media) {
    var res = "fa fa-fw ";
    res += (/視聴|CD|ＣＤ|AV|ＡＶ/i.test(media)) ? "fa-music" : "fa-book";
    return res;
};

rivets.formatters.unescapeHTML = function(str) {
    return str
            .replace(/&lt;/g, "<")
            .replace(/&gt;/g, ">")
            .replace(/&quot;/g, "\"")
            .replace(/&amp;/g, "&");
};

rivets.formatters.showAvailable = function(available) {
    switch(available) {
        case true: return "利用可";
        case false: return "貸出中";
        default: return "";
    }
};

rivets.formatters.formatDate = function(date) {
    return date.replace(".", "/");
};

rivets.binders.toggle = {
    bind: function(el) {
        var adapter = rivets.adapters["."];

        var binding = this;
        el.addEventListener("click", function() {
            var value = adapter.read(binding.model, binding.keypath);
            adapter.publish(binding.model, binding.keypath, !value);
        });
    },

    routine: function(el, value) {

    },
};
