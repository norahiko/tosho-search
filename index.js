"use strict";

var express = require("express");
var search = require("./lib/search.js");
var app = express();

if(app.get("env") === "development") {
    app.locals.pretty = true;
}

app.set("view engine", "jade");
app.set("port", Number(process.env.PORT || 8080));
app.use("/static", express.static(__dirname + "/static"));
app.use("/", express.static(__dirname + "/views"));

app.get("/search", function(req, res) {
    var id = req.query.id;
    var keyword = req.query.keyword;

    search.search(id, keyword, function(result) {
        if(result.ok === false) {
            console.error(result);
            res.status(400);
        }
        res.send(result);
    });
});

app.listen(app.get("port"), function() {
    console.log("Running at ", app.get("port"));
});
