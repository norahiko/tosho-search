"use strict";

var search = require("../lib/search.js");
var chai = require("chai");
var request = require("request");
var assert = chai.assert;

suite("Search service", function() {
    Object.keys(search.libraries).forEach(function(id) {
        var library = search.libraries[id];
        if(library.notSupported) {
            test.skip(id);
        } else {
            test(id, function(done) {
                this.slow(5000);
                this.timeout(15000);
                testLibrary(id, library, done);
            });
        }
    });
});


function testLibrary(id, library, done) {
    search.search(id, "ビートルズ", function(result) {
        if(result.ok === false) {
            console.log(result.error);
            assert.fail("request failed");
        }
        if(id === "musashino") {
            assert.typeOf(result.books, "array");
            done();
            return;
        }
        assert(1 < result.total, "result.totalは1以上の数値");
        assert(result.searchURL, "searchURL");

        var book = result.books[0];
        assert.typeOf(book.media, "string");
        assert(book.title, "book.title");
        assert.typeOf(book.author, "string");
        assert.typeOf(book.publisher, "string");
        assert.typeOf(book.publishedAt, "string");

        if(book.available !== null && typeof book.available !== "boolean") {
            console.log(book);
            assert.fail("book.available");
        }


        if(book.detail === "") {
            done();
            return;
        }

        var option = {
            url: book.detail,
            encoding: null,
            headers: {
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.94 Safari/537.36",
            }
        };

        request.get(option, function(error, headers, buffer) {
            assert.isNull(error);
            var html = library.decode(buffer, library.encoding);

            if(html.indexOf("詳細") === -1) {
                console.error(book);
                throw new Error("book.detailのリンクが利用できない");
            }
            done();
        });
    });
}

