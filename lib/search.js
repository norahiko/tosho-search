"use strict";

var iconv = require("iconv-lite");
var jaCodeMap = require("jaCodeMap");
var request = require("request");
var encodingjs = require("./encoding.js");


exports.search = function search(id, keyword, callback) {
    var service = libraries[id];
    if(service === undefined) {
        callback({ ok: false, books: [], error: "Invalid argument" });
        return;
    }
    service.request(keyword, callback);
};


var baseLibrary = {
    domain: "",
    searchPath: "",
    detailPath: "",
    method: "GET",
    encoding: "utf8",
    totalRegex: /該当件数[^\d]+(\d+)/,

    decode: function(str) {
        return decode(str, this.encoding);
    },

    getSearchURL: function(key) {
        var searchURLBase = this.domain + "/" + this.searchPath;
        return searchURLBase + "?" + this.query.replace("{keyword}", key);
    },

    getDetailURL: function(id) {
        return joinURL(this.domain + "/" + this.detailPath, id);
    },

    parse: function(html) {
        throw new Error("Not implemented");
    },

    request: function(keyword, callback) {
        var lib = this;
        var encoding = this.encoding;
        var key = encode(keyword, encoding);
        var searchURL = this.getSearchURL(key);
        var requestOption = {
            method: this.method,
            url: searchURL,
            encoding: null,
            timeout: 15 * 1000,
            jar: request.jar(),
            headers: {
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) by tosho-search.herokuapp.com",
            },
        };

        request(requestOption, function(err, response, buffer) {
            if(err) {
                callback({ ok: false, books: [], error: "接続エラー: " + err.toString() });
                return;
            }

            var html = decode(buffer, encoding);

            try {
                var books = lib.parse(html);
                var result = new SearchResult(books, null);
                var matchTotal = lib.totalRegex.exec(html);
                if(matchTotal) {
                    result.total = parseInt(matchTotal[1], 10);
                }
            } catch(error) {
                result = new SearchResult([], error);
            }

            result.searchURL = searchURL;
            callback(result);
        });
    }
};


function SearchResult(books, error) {
    this.books = [];
    if(error) {
        this.ok = false;
        this.error = error.toString();
    } else {
        this.ok = true;
        this.setBooks(books);
    }
}

SearchResult.prototype.setBooks = function(books) {
    books = books.slice(0, 20);
    this.books = books.map(function(b) {
        return {
            media: stripTag(b[0]),
            title: stripTag(b[1]),
            detail: b[2],
            author: stripTag(b[3]),
            publisher: stripTag(b[4]),
            publishedAt: stripTag(b[5]),
            available: b[6],
        };
    });
};


var libraries = exports.libraries = {};

var notSupportedLibrary = extend(baseLibrary, {
    notSupported: true,
    request: function(keyword, callback) {
        callback({
            ok: false,
            error: "現在、この図書館の検索は利用できません。"
        });
    }
});

libraries.edogawa = notSupportedLibrary;
libraries.shinagawa = notSupportedLibrary;
libraries.shibuya = notSupportedLibrary;
libraries.minato = notSupportedLibrary;
libraries.akishima = notSupportedLibrary;
libraries.chiyoda = notSupportedLibrary;


libraries.nakano = extend(baseLibrary, {
    notSupported: true,
    domain: "http://www3.city.tokyo-nakano.lg.jp",
    searchPath: "tosho/asp/xBook_Kensaku_w.asp",
    detailPath: "tosho/asp",
    query: "key1={keyword}&hidKensakuF=1&Page=1&rtkensaku=xkensaku_w.asp&tzn=1&sel1=1&SortKubun=4",
    encoding: "shift_jis",
    totalRegex: /検索結果は[^\d]+(\d+)/,

    parse: function(html) {
        html = html.slice(html.indexOf("検索結果は"));
        var rows = collectRegex(/<tr>([\s\S]*?)<\/tr>/ig, html).slice(1);

        return rows.map(function(row) {
            var cols = collectRegex(/<td[^>]*>([\s\S]+?)<\/td>/ig, row);
            var detail = this.getDetailURL(extractLink(cols[1]));
            return [cols[6], cols[1], detail, cols[2], cols[3], [4], null];
        }.bind(this));
    },
});


libraries.sumida = extend(baseLibrary, {
    domain: "https://www.library.sumida.tokyo.jp",
    searchPath: "totalresult",
    query: "key={keyword}&mv=20&sort=5&pcnt=1&reqsch=0",
    totalRegex: /検索結果[^\d]+(\d+)/,

    parse: function(html) {
        var rows = html.split("div class=\"infotable\"").slice(1);

        return rows.map(function(row) {
            var titleTag = /<a href=.+?<\/a>/.exec(row)[0];
            var detailLink = extractLink(titleTag).replace(/&amp;/g, "&").replace(/;jsessionid=\w+/, "");
            var cols = row.match(/<dd>.*?<\/dd>/g);
            if(cols.length === 3) {
                // 著者名が空欄
                cols.push(cols[2]);
                cols[2] = "";
            }

            var detail = this.getDetailURL(detailLink);
            return [cols[0], titleTag, detail, cols[2], cols[3], cols[1], null];
        }.bind(this));
    },
});


libraries.chuo = extend(libraries.sumida, {
    domain: "http://www.library.city.chuo.tokyo.jp",
});


libraries.nishitokyo = extend(libraries.sumida, {
    domain: "http://www.library.city.chuo.tokyo.jp",
});

libraries.toshima = extend(libraries.sumida, {
    domain: "http://www.library.toshima.tokyo.jp",
});

libraries.nishitokyo = extend(libraries.sumida, {
    domain: "http://www.library.city.nishitokyo.lg.jp",

    parse: function(html) {
        html = html.slice(html.indexOf("書誌１件分 start"));
        var rows = collectRegex(/<tr>([\s\S]*?)<\/tr>/ig, html).slice(1);

        return rows.map(function(row) {
            var cols = collectRegex(/<td[^>]*>([\s\S]*?)<\/td>/ig, row);
            var detail = extractLink(cols[2]).replace(/&amp;/g, "&").replace(/;jsessionid=\w+/, "");
            detail = this.getDetailURL(detail);
            return [cols[1], cols[2], detail, cols[3], cols[4], cols[5], null];
        }.bind(this));
    }
});

libraries.shinjuku = extend(baseLibrary, {
    domain: "https://www.library.shinjuku.tokyo.jp",
    searchPath: "opac/cgi-bin/sellist",
    query: "type=0&allc=&page=1&keyword={keyword}&sentaku=and&title=&sentaku=and&author=&sentaku=or&publish=&sentaku=or&isbn=&pubydate1=&pubydate3=&bunrui=&syubetu=all&kan=all&media=all&count=25&order=publish&before=select&authorid=&opacfile=&titleid=&tophp=select",
    detailPath: "opac/cgi-bin",
    totalRegex: /検索結果[^\d]+(\d+)/,
    encoding: "euc-jp",

    parse: function(html) {
        html = html.slice(html.indexOf("<table class=\"list\">"));
        var rows = collectRegex(/<tr>([\s\S]*?)<\/tr>/ig, html).slice(1);

        return rows.map(function(row) {
            var cols = collectRegex(/<td[^>]*>([\s\S]*?)<\/td>/ig, row);
            var detail = this.getDetailURL(extractLink(cols[1]));
            return [cols[6], cols[1], detail, cols[2], cols[3], cols[4], cols[5] !== "貸出中"];
        }.bind(this));
    },
});

libraries.adachi = extend(baseLibrary, {
    domain: "https://www.lib.adachi.tokyo.jp",
    searchPath: "licsxp-opac/WOpacTifSchCmpdExecAction.do",
    detailPath: "licsxp-opac",
    query: "kensaku_keyword={keyword}&chkflg=nocheck&loccodschkflg=nocheck&langcodschkflg=nocheck&currentSubSystem=0&disporder=7" +
           "&WebLinkFlag=1&moveToGamenId=tifschcmpdpre&SchType=1&dispmaxnum=20" ,

    parse: function(html) {
        if(html.indexOf("該当件数が1000件を超えました") !== -1) {
            throw new Error("該当件数が1000件を超えました。検索キーを見直し下さい。");
        }

        html = removeHTMLComment(html);
        var rows = collectRegex(/<tr class="ItemNo [^>]*>([\s\S]*?)<\/tr>/ig, html);
        return rows.map(function(row) {
            var cols = collectRegex(/<td[^>]*>([\s\S]*?)<\/td>/ig, row);
            return this.createBookData(cols);
        }.bind(this));
    },

    createBookData: function(cols) {
        var detail = this.getDetailURL(extractLink(cols[1])).replace("urlNotFlag=1&", "");
        return [cols[0], cols[1], detail, cols[2], cols[3], cols[4], cols[6] === "○"];
    },
});

libraries.suginami = extend(libraries.adachi, {
    domain: "https://www.library.city.suginami.tokyo.jp",

    createBookData: function(cols) {
        var detail = this.getDetailURL(extractLink(cols[1])).replace("urlNotFlag=1&", "");
        return [cols[0], cols[1], detail, cols[3], cols[4], cols[5], cols[7] === "○"];
    },
});

libraries.itabashi = extend(libraries.adachi, {
    domain: "https://www.lib.city.itabashi.tokyo.jp",
});

libraries.musashino = extend(libraries.adachi, {
    method: "POST",
    domain: "https://www.library.musashino.tokyo.jp",
});

libraries.fuchu = extend(libraries.adachi, {
    domain: "https://library.city.fuchu.tokyo.jp",
});

libraries.higashikurume = extend(libraries.suginami, {
    domain: "https://www.lib.city.higashikurume.lg.jp",
});

libraries.kita = extend(baseLibrary, {
    domain: "http://www.library.city.kita.tokyo.jp",
    searchPath: "OPP1400",
    query: "WORD={keyword}&MENUNO=0&SP_SEARCH=1&TERM=INDEX-0&ORDER=DESC&ORDER_ITEM=SORT4-F",
    totalRegex: /件目／[^\d]+(\d+)/,

    parse: function(html) {
        html = html.slice(html.indexOf("項番"));
        html = html.slice(0, html.indexOf("＜＜前ページ"));
        var rows = collectRegex(/<TD nowrap VALIGN="top">([\s\S]*?)<\/TD>/g, html);

        return rows.map(function(row) {
            row = jaCodeMap.f2hSym(jaCodeMap.auto(stripTag(row)));
            var title = row.slice(0, row.indexOf("/"));
            row = row.slice(row.indexOf("/") + 1);
            var pi = row.lastIndexOf(";");
            var auth = row.slice(0, pi).trim();
            var author = auth.slice(0, auth.lastIndexOf(" "));
            var publisher = auth.slice(author.length + 1);
            var publishedAt = /\d*\.\d*/.exec(row.slice(pi))[0];
            return ["図書", title, "", author || "", publisher, publishedAt, null];
        });
    },
});


libraries.arakawa = extend(baseLibrary, {
    domain: "http://www.library.city.arakawa.tokyo.jp",
    searchPath: "clis/search",
    query: "KEY1={keyword}&BOOK=ON&MAGAZINE=ON&AV=ON&ITEM1=AB&COMP1=3&ITEM2=CD&COND=1&SORT=5&MAXVIEW=50",
    encoding: "shift_jis",
    parse: function(html) {
        html = html.slice(html.indexOf("<TBODY>"));
        var rows = collectRegex(/<TR>([\s\S]*?)<\/TR>/ig, html);
        return rows.map(function(row) {
            var cols = collectRegex(/<\/?TD[^>]*>(.*?)<\/TD>/ig, row);
            var detail = extractLink(cols[2]).replace(/SID=.*/, "");
            return [cols[1], cols[2], joinURL(this.domain, detail), cols[3], cols[4], cols[5], null];
        }.bind(this));
    },
});


libraries.ota = extend(libraries.arakawa, {
    domain: "http://www.lib.city.ota.tokyo.jp",
});

libraries.katsushika = extend(libraries.arakawa, {
    domain: "http://www.lib.city.katsushika.lg.jp",
});

libraries.setagaya = extend(libraries.arakawa, {
    domain: "http://libweb.city.setagaya.tokyo.jp",
    searchPath: "cgi-bin/search",
});

libraries.mitake = extend(libraries.arakawa, {
    domain: "http://www.library.mitaka.tokyo.jp",
});

libraries.tama = extend(libraries.arakawa, {
    domain: "http://www.library.tama.tokyo.jp",
});

libraries.chofu = extend(libraries.arakawa, {
    domain: "https://www.lib.city.chofu.tokyo.jp",
});

libraries.nerima = extend(baseLibrary, {
    domain: "http://www.lib.nerima.tokyo.jp",
    searchPath: "opw/OPW/OPWSRCHLIST.CSP",
    query: "text(1)={keyword}&DB=LIB&FLG=SEARCH&MODE=1&SORT=-3&PID2=OPWSRCH2&opr(1)=OR&qual(1)=ALL&WRTCOUNT=20&av=on",
    totalRegex: /件数[^\d]+(\d+)/,

    parse: function(html) {
        var rows = collectRegex(/<TR class=(?:lightcolor|basecolor)>([\s\S]*?)<\/TR>/g, html);
        return rows.map(function(row) {
            var cols = collectRegex(/<\/?TD[^>]*>(.*?)<\/TD>/g, row);
            return this.createBookData(cols);
        }.bind(this));
    },

    createBookData: function(cols) {
        return [cols[1], cols[2], extractLink(cols[2]), cols[3], cols[4], cols[5], null];
    }
});

libraries.koto = extend(libraries.nerima, {
    domain: "http://www.koto-lib.tokyo.jp",
});

libraries.hachioji = extend(libraries.nerima, {
    domain: "http://www.library.city.hachioji.tokyo.jp",
});

libraries.meguro = extend(libraries.nerima, {
    domain: "http://www.meguro-library.jp",
    createBookData: function(cols) {
        return ["", cols[1], extractLink(cols[1]), cols[2], cols[3], cols[4], null];
    }
});

libraries.bunkyo = extend(libraries.nerima, {
    domain: "http://www.lib.city.bunkyo.tokyo.jp",
    query: "text(1)={keyword}&DB=LIB&PID2=OPWSRCH2&FLG=SEARCH&MODE=1&SORT=-3&opr(1)=OR&qual(1)=MZALL",
});

function joinURL(base, path) {
    if(base[base.length - 1] === "/") {
        base = base.slice(0, -1);
    }
    path = path.replace(/^\.?\//, "");
    return base + "/" + path;
}

function collectRegex(regex, text) {
    // jshint boss: true
    var result = [];
    var match;
    while(match = regex.exec(text)) {
        result.push(match[1]);
    }
    return result;
}

function stripTag(html) {
    html = html || "";
    return stripWhiteSpaces(html.replace(/<[^>]+>/g, ""));
}

function stripWhiteSpaces(text) {
    return text.replace(/&nbsp;|[\t\r\n\u3000]/g, " ").replace(/ +/g, " ").replace(/^\s+|\s+$/g, "");
}

function extractLink(html) {
    var match = /href=["']([^"']+)["']/i.exec(html);
    return match ? match[1] : "";
}

function removeHTMLComment(html) {
    return html.replace(/<!--[\s\S]*?-->/g, "");
}


function encode(str, encoding) {
    return encoding === "utf8" ?
            encodeURIComponent(str) :
            encodingjs.urlEncode(iconv.encode(str, encoding));
}

function decode(buffer, encoding) {
    return encoding === "utf8" ?
            buffer.toString() :
            iconv.decode(buffer, encoding);
}

function extend(base, sub) {
    var extended = Object.create(base);
    for(var k in sub) {
        extended[k] = sub[k];
    }
    return extended;
}
